package com.longsong.mqtt_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;


import java.util.ArrayList;
import java.util.List;

public class ChartActivity extends AppCompatActivity {


    private LineChart line;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_layout);
        line = (LineChart) findViewById(R.id.line_chart);
        if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//横屏
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏
        }

        Intent intent=getIntent();//链接主页面

        List<Entry> list_wd= intent.getParcelableArrayListExtra("list1");
        List<Entry> list_sd= intent.getParcelableArrayListExtra("list2");


//list是你这条线的数据  label 是你对这条线的描述（也就是图例上的文字）
        List<ILineDataSet> LineData_list = new ArrayList<>();
        LineDataSet lineDataSet_sd=new LineDataSet(list_sd,"温度");
        LineDataSet lineDataSet_wd=new LineDataSet(list_wd,"温度");
        LineData_list.add(lineDataSet_sd);
        LineData_list.add(lineDataSet_wd);
        LineData lineData=new LineData(LineData_list);

        line.setData(lineData);

//简单美化

        XAxis xAxis = line.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); // 设置X轴的位置
        xAxis.setDrawAxisLine(true);
        line.getAxisRight().setEnabled(false);
        xAxis.setEnabled(true);
        xAxis.setDrawGridLines(false); // 效果如下图

    }
}